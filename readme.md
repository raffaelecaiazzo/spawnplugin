# Spawn Plugin

#### [Download Plugin (SpigotMC)](https://www.spigotmc.org/resources/spawnplugin-1-12-1-19.94841/)

> ### Native Minecraft Version: 1.12
> ### Tested Minecraft Versions: 1.12, 1.13, 1.14, 1.15, 1.16, 1.17, 1.18, 1.19

**► What can you do with this plugin:** this plugin allows you to set the spawn and you also have the possibility to enable the Spawn On Join option in the config (Enabled by Default).

**► Installation:** to install the plugin all you need to do is:
- Download it
- Place it in your Plugins folder in your minecraft server
- Restart/Reload the server

**► Permissions:**
- **spawn.admin** (Allows you to set the spawn)
- **spawn.spawn** (Allows you to go to the spawn)

**► Commands:**
- **/spawn** (Go to the spawn))
- **/setspawn** (Set the spawn)
