package me.raffaele.spawnplugin;

import me.raffaele.spawnplugin.events.spawnOnJoin;
import me.raffaele.spawnplugin.executors.setSpawn;
import me.raffaele.spawnplugin.executors.spawn;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class SpawnPlugin extends JavaPlugin {

    public static SpawnPlugin plugin;

    @Override
    public void onEnable() {
        plugin = this;
        System.out.print("[SpawnPlugin] Plugin Abilitato Correttamente!");
        this.saveDefaultConfig();
        getCommand("spawn").setExecutor(new spawn());
        getCommand("setspawn").setExecutor(new setSpawn());
        Bukkit.getPluginManager().registerEvents(new spawnOnJoin(), this);
    }

    @Override
    public void onDisable() {
        System.out.print("[SpawnPlugin] Plugin Disabilitato Correttamente!");
    }

    public static SpawnPlugin getInstance(){
        return plugin;
    }
}
