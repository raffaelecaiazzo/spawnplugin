package me.raffaele.spawnplugin.events;

import me.raffaele.spawnplugin.SpawnPlugin;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class spawnOnJoin implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        SpawnPlugin.getInstance().reloadConfig();

        if(!e.getPlayer().hasPlayedBefore()){
            if (SpawnPlugin.getInstance().getConfig().getString("spawn.world") != null && SpawnPlugin.getInstance().getConfig().getString("spawn.x") != null && SpawnPlugin.getInstance().getConfig().getString("spawn.y") != null && SpawnPlugin.getInstance().getConfig().getString("spawn.z") != null && SpawnPlugin.getInstance().getConfig().getString("spawn.yaw") != null && SpawnPlugin.getInstance().getConfig().getString("spawn.pitch") != null){
                World world = Bukkit.getWorld(SpawnPlugin.getInstance().getConfig().getString("spawn.world"));
                double x = SpawnPlugin.getInstance().getConfig().getDouble("spawn.x");
                double y = SpawnPlugin.getInstance().getConfig().getDouble("spawn.y");
                double z = SpawnPlugin.getInstance().getConfig().getDouble("spawn.z");
                float yaw = (float) SpawnPlugin.getInstance().getConfig().getDouble("spawn.yaw");
                float pitch = (float) SpawnPlugin.getInstance().getConfig().getDouble("spawn.pitch");

                Location loc = new Location(world, x, y, z, yaw, pitch);
                e.getPlayer().teleport(loc);
            }
        }else{
            if(Boolean.parseBoolean(SpawnPlugin.getInstance().getConfig().getString("settings.spawn-on-join"))){
                if (SpawnPlugin.getInstance().getConfig().getString("spawn.world") != null && SpawnPlugin.getInstance().getConfig().getString("spawn.x") != null && SpawnPlugin.getInstance().getConfig().getString("spawn.y") != null && SpawnPlugin.getInstance().getConfig().getString("spawn.z") != null && SpawnPlugin.getInstance().getConfig().getString("spawn.yaw") != null && SpawnPlugin.getInstance().getConfig().getString("spawn.pitch") != null){
                    World world = Bukkit.getWorld(SpawnPlugin.getInstance().getConfig().getString("spawn.world"));
                    double x = SpawnPlugin.getInstance().getConfig().getDouble("spawn.x");
                    double y = SpawnPlugin.getInstance().getConfig().getDouble("spawn.y");
                    double z = SpawnPlugin.getInstance().getConfig().getDouble("spawn.z");
                    float yaw = (float) SpawnPlugin.getInstance().getConfig().getDouble("spawn.yaw");
                    float pitch = (float) SpawnPlugin.getInstance().getConfig().getDouble("spawn.pitch");

                    Location loc = new Location(world, x, y, z, yaw, pitch);
                    e.getPlayer().teleport(loc);
                }
            }
        }
    }

}
