package me.raffaele.spawnplugin.executors;

import me.raffaele.spawnplugin.SpawnPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class setSpawn implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player){
            Player p = (Player) sender;
            if(args.length == 0){
                if(sender.isOp() || sender.hasPermission("spawn.admin")){
                    SpawnPlugin.getInstance().reloadConfig();
                    SpawnPlugin.getInstance().getConfig().set("spawn.world", p.getWorld().getName());
                    SpawnPlugin.getInstance().getConfig().set("spawn.x", p.getLocation().getX());
                    SpawnPlugin.getInstance().getConfig().set("spawn.y", p.getLocation().getY());
                    SpawnPlugin.getInstance().getConfig().set("spawn.z", p.getLocation().getZ());
                    SpawnPlugin.getInstance().getConfig().set("spawn.yaw", p.getLocation().getYaw());
                    SpawnPlugin.getInstance().getConfig().set("spawn.pitch", p.getLocation().getPitch());
                    SpawnPlugin.getInstance().saveConfig();
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', SpawnPlugin.getInstance().getConfig().getString("messages.success-setspawn")));
                }else{
                    p.sendMessage("§cYou do not have permission to execute this command!");
                }
            }else{
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', SpawnPlugin.getInstance().getConfig().getString("messages.cmd-setspawn-usage")));
            }
        }else{
            sender.sendMessage("This Command can only executed by a player, sorry!");
        }

        return false;
    }
}
